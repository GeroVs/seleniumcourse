package claseUno;

import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class SwitchWindows {

	static WebDriver driver;
	public static void main(String[] args) {
		
		WebElement element;
		Select s;
		JavascriptExecutor js;
		
		String driverPath = "C:\\Selenium\\chromedriver_win32\\chromedriver.exe";
		System.setProperty("webdriver.chrome.drive", driverPath);
		
		driver = new ChromeDriver();
		js = ((JavascriptExecutor)driver);
		
		try {
			driver.navigate().to("http://demoqa.com/frames-and-windows/");
		}catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		element = driver.findElement(By.id("ui-id-2"));
		element.click();
		
		element = driver.findElement(By.xpath("//a[text()= \"Open New Seprate Window\"]"));
		element.click();
		
		SwitchToWindow();
		
		element = driver.findElement(By.className("dt-mobile-menu-icon"));
		element.click();
	}
	
	public static void SwitchToWindow() {
		String curwin = driver.getWindowHandle();
		Set<String> windows = driver.getWindowHandles();
		windows.remove(curwin);
		
		for(String x : windows) {
			if (x != curwin) {
				driver.switchTo().window(x);
				}
			}
	}
}
