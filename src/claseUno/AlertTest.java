package claseUno;

import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class AlertTest {
	static WebDriver driver;
	public static void main(String[] args) throws InterruptedException {

			
			WebElement element;
			Select s;
			JavascriptExecutor js;
			
			String driverPath = "C:\\Selenium\\chromedriver_win32\\chromedriver.exe";
			System.setProperty("webdriver.chrome.drive", driverPath);
			
			driver = new ChromeDriver();
			js = ((JavascriptExecutor)driver);
			
			try {
				driver.navigate().to("https://www.w3schools.com/jsref/tryit.asp?filename=tryjsref_alert");
			}catch (Exception e) {
				System.out.println(e.getMessage());
			}
			
			driver.switchTo().frame("iframeResult");
			
			element = driver.findElement(By.xpath("//button[text() = \"Try it\"]"));
			element.click();
			
			driver.switchTo().alert().accept();
		}
		
		public static void SwitchToWindow() {
			String curwin = driver.getWindowHandle();
			Set<String> windows = driver.getWindowHandles();
			windows.remove(curwin);
			
			for(String x : windows) {
				if (x != curwin) {
					driver.switchTo().window(x);
					}
				}
		}

	}
