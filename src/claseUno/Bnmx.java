import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
//import org.openqa.selenium.support.ui.WebDriverWait;

public class Bnmx {

	public static void main(String[] args) throws InterruptedException {
		
		WebDriver driver;
		WebElement element;
		Select s;
		JavascriptExecutor js;
		
		String driverPath = "C://Selenium//chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", driverPath);
	
		driver = new ChromeDriver();
	
		driver.manage().window().maximize();
		js = ((JavascriptExecutor)driver);
		
		
		//WebDriverWait varWait = new WebDriverWait(driver, 10);
		
		
		try {
			
			driver.navigate().to("https://www.segurosbanamex.com.mx/segurosBanamex/seguro-auto-banamex-contratacion-paso-uno.jsp");
			
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}
		
		
		js.executeScript("window.scrollBy(0,500);");
		
		// id vehiculo By value AUT
		element = driver.findElement(By.id("vehiculo"));
		s = new Select(element);
		s.selectByValue("AUT");
		
		Wait(5);
		element = driver.findElement(By.id("marca"));
		s = new Select(element);
		s.selectByVisibleText("PORSCHE");
				
		

	}


	public static void Wait(int s) throws InterruptedException {
		Thread.sleep(s*1000);
	}


}
