
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class GSearch {

	public static void main(String[] args) throws InterruptedException {

		WebDriver driver;
		WebElement element;

		String driverPath = "D:\\Cpcty\\geckodriver-v0.21.0-win32\\geckodriver.exe";
		System.setProperty("webdriver.firefox.bin",
                "C:\\Users\\42738\\AppData\\Local\\Mozilla Firefox\\firefox.exe");
		System.setProperty("webdriver.gecko.driver", driverPath);
		//DesiredCapabilities capability = DesiredCapabilities.firefox();
		//capability.setCapability("platform", Platform.ANY);
		//capability.setCapability("binary", "C:\\Users\\42738\\AppData\\Local\\Mozilla Firefox\\firefox.exe"); //for linux

		driver = new FirefoxDriver();

		driver.manage().window().maximize();


		// Navigate
		try {

			driver.navigate().to("https://www.google.com");
 
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}
		// Get
		//driver.get("https://www.google.com");

		// Buscar Hexaware
		element = driver.findElement(By.name("q"));
		element.sendKeys("Hexaware");
		element.sendKeys(Keys.ENTER);

		Wait(3);
		
		element = driver.findElement(By.xpath("//a[contains(text(), \"Hexaware - IT, BPO\")]"));
		element.click();		
		driver.quit();
	}	
	public static void Wait(int s) throws InterruptedException {
		Thread.sleep(s* 1000);

	}

}

