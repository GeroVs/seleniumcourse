import org.openqa.selenium.By;
//import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Dmnos {

	public static void main(String[] args) throws InterruptedException {
		WebDriver driver;
		WebElement element;
		Select s;
		
		
		String driverPath = "D:\\Cpcty\\geckodriver-v0.21.0-win32\\geckodriver.exe";
		System.setProperty("webdriver.firefox.bin",
                "C:\\Users\\42738\\AppData\\Local\\Mozilla Firefox\\firefox.exe");
		System.setProperty("webdriver.gecko.driver", driverPath);
	
		driver = new FirefoxDriver();
		//driver.manage().window().setSize(new Dimension(450,400));
		WebDriverWait wait = new WebDriverWait(driver, 90);
		JavascriptExecutor js = ((JavascriptExecutor)driver);
		
		try {
			
			driver.navigate().to("http://dominos.com.mx/");
			
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}
		
		element = driver.findElement(By.cssSelector("button.btn-green.hideXSDP"));
		element.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("pac-input")));
		element= driver.findElement(By.id("pac-input"));
		element.sendKeys("Saltillo");
		element.sendKeys(Keys.ENTER);
		Wait(5);
		element= driver.findElement(By.id("btnContinue"));
		element.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("streetName")));
		element= driver.findElement(By.id("streetName"));
		element.clear();
		element.sendKeys("Avenida San Angel");
		Wait(1);
		element = driver.findElement(By.id("streetNumber"));
		element.sendKeys("240");
		Wait(1);
		element = driver.findElement(By.id("deliveryInstructions"));
		element.sendKeys("Hexaware");
		Wait(1);
		element = driver.findElement(By.id("postalCode"));
		element.clear();
		Wait(1);
		element.sendKeys("25215");
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("preloader")));
		element = driver.findElement(By.id("btnContinue"));
		element.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@href=\"../menu/specialtyCatalog.php\"]")));
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("preloader")));	
		element = driver.findElement(By.xpath("//a[@href=\"../menu/specialtyCatalog.php\"]"));
		element.click();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("preloader")));
		element = driver.findElement(By.xpath("//img[contains(@src, \"/HNC.png\")]"));
		element.click();
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("preloader")));
		element = driver.findElement(By.id("flavorCode"));
		s = new Select (element);
		s.selectByValue("SARTEN");
		
		element = driver.findElement(By.id("sizeCode"));
		s = new Select(element);
		s.selectByValue("14");
		
		element = driver.findElement(By.id("btnAdd2Cart"));
		element.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".close")));
		element = driver.findElement(By.cssSelector(".close"));
		element.click();
		
		try {
			element = driver.findElement(By.cssSelector(".closeTutorialCar"));
			element.click();
		} catch (Exception e) {
			e.printStackTrace();
		}
		Wait(3);
		element = driver.findElement(By.id("linkPayCart"));
		element.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("firstName")));
		
		
		element = driver.findElement(By.id("firstName"));
		element.sendKeys("Ketsu");
		
		element = driver.findElement(By.id("lastName"));
		element.sendKeys("Vlds");
		
		element = driver.findElement(By.id("phone"));
		element.sendKeys("0448444000000");
		
		element = driver.findElement(By.id("email"));
		element.sendKeys("mail@domain.com");
		
		element = driver.findElement(By.xpath("//label[@for = \"EmailOptIn\" ]"));
		element.click();
		
		element = driver.findElement(By.xpath("//label[@for = \"AgreeToTermsOfUse\"]"));
		element.click();
		
		element = driver.findElement(By.xpath("//label[@for = \"Age13OrOlder\"]"));
		element.click();
		
		element = driver.findElement(By.xpath("//label[@for = \"paymentType_Cash\"]"));
		element.click();
		
		element = driver.findElement(By.id("btnSubmitOrder"));
		js.executeScript("arguments[0].scrollIntoView(true);", element);
	}

	public static void Wait(int s) throws InterruptedException {
		Thread.sleep(s* 1000);

	}

}
