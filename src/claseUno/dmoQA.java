import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class DQA {

	public static void main(String[] args) {
	
		WebDriver driver;
		WebElement element;
		Select s;
		
		String driverPath = "D:\\Cpcty\\geckodriver-v0.21.0-win32\\geckodriver.exe";
		System.setProperty("webdriver.firefox.bin",
                "C:\\Users\\42738\\AppData\\Local\\Mozilla Firefox\\firefox.exe");
		System.setProperty("webdriver.gecko.driver", driverPath);
	
		driver = new FirefoxDriver();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		driver.manage().window().maximize();
		
		
		try {
			
			driver.navigate().to("https://www.demoqa.com/registration");
			
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}
		
		// id name_3_firstname 
		element = driver.findElement(By.id("name_3_firstname"));
		element.sendKeys("Gerardo");
		
		// id name_3_lastname 
		element = driver.findElement(By.id("name_3_lastname"));
		element.sendKeys("Valdes");
		
		// xpath //*[@value="single"] 
		element = driver.findElement(By.xpath("//*[@value=\"single\"]"));
		element.click();
		
		// xpath (//*[@name="checkbox_5[]"])[2]
		element = driver.findElement(By.xpath("(//*[@name=\"checkbox_5[]\"])[2]"));
		element.click();
		
		
		// Select Country id=> dropdown_7 Mexico
		element = driver.findElement(By.id("dropdown_7"));
		s = new Select(element);
		s.selectByVisibleText("Mexico");
		
		// id mm_date_8 month => 7
		
		element = driver.findElement(By.id("mm_date_8"));
		s = new Select(element);
		s.selectByIndex(6);
		
		// id dd_date_8 day=> 8
		element = driver.findElement(By.id("dd_date_8"));
		s = new Select(element);
		s.selectByVisibleText("22");
		
		// id yy_date_8 year => 1994
		element = driver.findElement(By.id("yy_date_8"));
		s = new Select(element);
		s.selectByValue("1992");;
		
		// phone_9 id 844 321 3454
		element = driver.findElement(By.id("phone_9"));
		element.sendKeys("(8446224549");
		
		
		// name username aaronlopez24
		element = driver.findElement(By.name("username"));
		element.sendKeys("GeroVS");
		
		
		// name => e_mail aaronlopez24@hotmail.com
		element = driver.findElement(By.name("e_mail"));
		element.sendKeys("gero.vlds@gmail.com");
		
		
		// id => password_2
		element = driver.findElement(By.id("password_2"));
		element.sendKeys("Gro-Vld$-22-07");
		
		// id => confirm_password_password_2 
		element = driver.findElement(By.id("confirm_password_password_2"));
		element.sendKeys("Gro-Vld$-22-07");
		
		
		// strong pass id = "piereg_passwordStrength" 
		element = driver.findElement(By.id("piereg_passwordStrength"));
		if(element.getText().equals("Strong")) {
			
			System.out.println("Contrase;a segura");
			js.executeScript("window.scrollBy(0,450)");
			element = driver.findElement(By.name("pie_submit"));
			element.click();
		}
	}
}

