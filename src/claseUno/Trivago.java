import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
public class Tvgo {

	static WebDriver driver;
	public static void main(String[] args) throws InterruptedException {
		
		WebElement element;
		Select s;
		
		String driverPath = "D:\\Cpcty\\geckodriver-v0.21.0-win32\\geckodriver.exe";
		System.setProperty("webdriver.firefox.bin",
                "C:\\Users\\42738\\AppData\\Local\\Mozilla Firefox\\firefox.exe");
		System.setProperty("webdriver.gecko.driver", driverPath);
	
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		
		try {
			
			driver.navigate().to("https://www.trivago.com/es");
			
		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}
		
		element = driver.findElement(By.id("horus-querytext"));
		element.sendKeys("Vancouver");
		element.sendKeys(Keys.ENTER);
		
		Wait(1);
		element = driver.findElement(By.cssSelector(".cal-btn-next"));
		element.click();
		Wait(2);
		element = driver.findElement(By.xpath("//time[@datetime= \"2018-10-14\"]"));
		element.click();
		Wait(1);
		element = driver.findElement(By.xpath("//time[@datetime= \"2018-10-22\"]"));
		element.click();
		Wait(2);
		element = driver.findElement(By.xpath("(//li[@role=\"menuitem\"])[1]"));
		element.click();
		Wait(1);
		element = driver.findElement(By.xpath("//button[text() = \"Ok\"]"));
		element.click();
		Wait(5);
		List<WebElement> elements = driver.findElements(By.xpath("//span[@data-log-id= \"1\" and contains(text(), \"$\")]"));
		
		int price = Integer.parseInt(elements.get(0).getText().substring(1));
		for (WebElement el : elements) {
			int vsPrice = Integer.parseInt(el.getText().substring(1).replace(",", ""));
	        if ( vsPrice < price) {
	            price = vsPrice;
	        }
	    }
		String pText = String.format("%,d", price);
		
		try {
			element = driver.findElement(By.xpath("//span[contains(text(),\"$"+pText+"\")]"));
			element.click();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			Wait(1);
			element = driver.findElement(By.className("alert__close"));
			element.click();
			Wait(1);
			element = driver.findElement(By.xpath("//span[contains(text(),\"$"+pText+"\")]"));
			element.click();
		}
		
		SwitchToWindows();
		
		element = driver.findElement(By.cssSelector("#listing-price-section-container-10585605 button"));
		element.click();
		SwitchToWindows();
		element = driver.findElement(By.cssSelector("#name-and-price button"));
		SwitchToWindows();
		 element = driver.findElement(By.id("firstName"));
		 element.sendKeys("Gerard");
		 
		 element = driver.findElement(By.id("lastName"));
		 element.sendKeys("Strauss");
		 
		 element = driver.findElement(By.id("billing-name-the-same"));
		 element.click();
		 
		 element = driver.findElement(By.id("address"));
		 element.sendKeys("Main st. #34565");
		 
		 element = driver.findElement(By.id("billingCountry"));
		 s = new Select(element);
		 s.selectByValue("MX");
		 
		 element = driver.findElement(By.id("city"));
		 element.sendKeys("Saltillo");
		 
		 element = driver.findElement(By.id("province"));
		 element.sendKeys("Coahuila");
		 
		 element = driver.findElement(By.id("zipcode"));
		 element.sendKeys("25000");
		 
		 element = driver.findElement(By.id("telephone1"));
		 element.sendKeys("+528441234567");
		 
		 element = driver.findElement(By.id("email-address"));
		 element.sendKeys("email@domain.com");
		 
		 element = driver.findElement(By.id("cc-number"));
		 element.sendKeys("0000553242681240");
		 
		 element = driver.findElement(By.id("card-expiration-month"));
		 s = new Select(element);
		 s.selectByValue("01");
		 
		 element = driver.findElement(By.id("card-expiration-year"));
		 s = new Select(element);
		 s.selectByValue("2019");
		 
		 element = driver.findElement(By.id("book-button"));
		 element.click();
		 }
	
public static void SwitchToWindows() {
		
		String currWin = driver.getWindowHandle();
		Set<String> windows = driver.getWindowHandles();
		windows.remove(currWin);
		
		for(String x : windows) {
			
			if(x != currWin)
				driver.switchTo().window(x);
			
		}
}


	public static void Wait(int s) throws InterruptedException {
		Thread.sleep(s* 1000);

	}
}
