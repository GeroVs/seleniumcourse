import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Orbitz {
	
	static WebDriver driver;

	public static void main(String[] args) {
		
		WebElement element;
		Select s;
		
		String driverPath = "C:\\Selenium\\chromedriver_win32\\chromedriver.exe";
		System.setProperty("webdriver.chrome.drive", driverPath);
		
		driver = new ChromeDriver();

		WebDriverWait wait = new WebDriverWait(driver, 15);

		try {
			driver.navigate().to("https://www.orbitz.com/");
		}catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		element = driver.findElement(By.id("package-origin-hp-package"));
		element.sendKeys("Monterrey");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("typeaheadDataPlain")));
		element.sendKeys(Keys.ENTER);
		
		element = driver.findElement(By.id("package-destination-hp-package"));
		element.sendKeys("Cancun");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("typeaheadDataPlain")));
		element.sendKeys(Keys.ENTER);
		
		element = driver.findElement(By.id("package-departing-hp-package"));
		element.sendKeys("09/15/2018");
		
		element = driver.findElement(By.id("package-returning-hp-package"));
		element.click();
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.className("datepicker-cal")));
		element = driver.findElement(By.xpath("//button[@data-month = \"8\" and @data-day = \"23\"]"));
		element.click();
		
		element = driver.findElement(By.id("package-rooms-hp-package"));
		s = new Select(element);
		s.selectByValue("1");
		
		element = driver.findElement(By.id("package-1-adults-hp-package"));
		s = new Select(element);
		s.selectByValue("1");
		
		element = driver.findElement(By.id("search-button-hp-package"));
		element.click();
		
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("page-interstitial")));
		
		element = driver.findElement(By.cssSelector("article:first-child"));
		element.click();
		
		SwitchToWindow();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#rooms-and-rates article:nth-child(2) .btn")));
		element = driver.findElement(By.cssSelector("#rooms-and-rates article:nth-child(2) .btn"));
		element.click();
		
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#flightModuleList li:first-child button")));
		element = driver.findElement(By.cssSelector("#flightModuleList li:first-child button"));
		element.click();
		
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#flightModuleList li:first-child button")));
		element = driver.findElement(By.cssSelector("#flightModuleList li:first-child button"));
		element.click();
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@id = \"FlightUDPBookNowButton1\"]/button")));
		element = driver.findElement(By.xpath("//div[@id = \"FlightUDPBookNowButton1\"]/button"));
		element.click();
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[contains(@name, \"titleIdAndName\")]")));
		element = driver.findElement(By.xpath("//*[contains(@name, \"titleIdAndName\")]"));
		s = new Select(element);
		s.selectByValue("1_Mr.");
		
		element = driver.findElement(By.id("firstname[0]"));
		element.sendKeys("Gerardo");
		
		element = driver.findElement(By.id("lastname[0]"));
		element.sendKeys("Strauss");
		
		element = driver.findElement(By.xpath("//select[contains(@name, \"phoneCountryCode\")]"));
		s = new Select(element);
		s.selectByValue("52");
		
		element = driver.findElement(By.xpath("//input[contains(@name, \"phoneNumber\")]"));
		element.sendKeys("8441002233");
		
		element = driver.findElement(By.cssSelector(".traveler-dob-month select"));
		s = new Select(element);
		s.selectByValue("07");
		
		element = driver.findElement(By.cssSelector(".traveler-dob-day select"));
		s = new Select(element);
		s.selectByValue("22");
		
		element = driver.findElement(By.cssSelector(".traveler-dob-year select"));
		s = new Select(element);
		s.selectByValue("1992");
		
		element = driver.findElement(By.id("no_insurance_package"));
		element.click();
		
		element = driver.findElement(By.id("creditCardInput"));
		element.sendKeys("5304421600020101");
		
		element = driver.findElement(By.cssSelector(".cc-expiry-month"));
		s = new Select(element);
		s.selectByValue("12");
		
		element = driver.findElement(By.cssSelector(".cc-expiry-year"));
		s = new Select(element);
		s.selectByValue("2018");
		
		element = driver.findElement(By.id("new_cc_security_code"));
		element.sendKeys("123");
		
		element = driver.findElement(By.name("country"));
		s = new Select(element);
		s.selectByValue("MEX");
		
		element = driver.findElement(By.cssSelector(".billing-address-one"));
		element.sendKeys("Avenida San angel 240");
		
		element= driver.findElement(By.name("zipcode"));
		element.sendKeys("25000");
		
		element = driver.findElement(By.name("city"));
		element.sendKeys("Saltillo");
		
		element = driver.findElement(By.name("state"));
		element.sendKeys("Coahuila");
		
		element = driver.findElement(By.name("email"));
		element.sendKeys("email@domain.com");
		
		element = driver.findElement(By.id("complete-booking"));
		element.click();

	}
	
	public static void SwitchToWindow() {
		String curwin = driver.getWindowHandle();
		Set<String> windows = driver.getWindowHandles();
		windows.remove(curwin);
		
		for(String x : windows) {
			if (x != curwin) {
				driver.switchTo().window(x);
				}
			}
	}

}
